import py2neo
import re
import pydot
# from https://stackoverflow.com/questions/14132789/relative-imports-for-the-billionth-time
if __package__ is None or __package__ == '':
    # uses current directory visibility
    import agents
    import Graphable
    # import python_aep_lib.agents as agents
else:
    # uses current package visibility
    import python_aep_lib.agents as agents
    import python_aep_lib.Graphable as Graphable
graph = py2neo.Graph("bolt://138.102.159.59:4205", auth=("neo4j", "pic3.14"))
# graph = py2neo.Graph("bolt://localhost", auth=("neo4j", "pic3.14"))


class AgressorHelper(Graphable.Graphable):
    agentHelper = agents.AgentHelper()
    def __init__(self):
        Graphable.Graphable.__init__(self)
    def findAgressors(self, agent, graphic = True):
        if graphic:
            nodeDict = {}
            edgeDict = {}
            self.graphBuffer = pydot.Dot(graph_type='digraph')
            nodeDict[agent['id']['name']] = {'style' : "filled", 'fillcolor' : "green"}
        origin_nodes = [agent['id']['name']]
        agressors = {}
        for sub in agent['subpart']:
            origin_nodes.append(sub['name'])
        stringified_origin_nodes = '["' + '", "'.join(origin_nodes) +'"]'
        cypher_query = graph.run(''' 
        MATCH (origin) <-[:involve {role: "defavorable"}]- (processus:LS_Processus) -[:involve]-> (agressor)
        WHERE origin.name IN ''' + stringified_origin_nodes + '''
        AND NOT agressor.name IN ''' + stringified_origin_nodes + '''
        RETURN origin, processus, collect(agressor) AS agressors
        ''').data()
        for datum in cypher_query:
            for _agressor in datum['agressors']:
                agressor_ref = AgressorHelper.agentHelper.rootAgentIdentityBindings[_agressor['name']]
                agressors[agressor_ref['id']['name']] = agressor_ref
        if graphic:
            for datum in cypher_query:
                    nodeDict[datum['origin']['name']] = {'color': "green"}
                    if datum['origin']['name'] != agent['id']['name']:
                        edgeDict[datum['origin']['name'] + '_' + agent['id']['name']] = {'src': datum['origin']['name'], 'dst': agent['id']['name'], 'label':"fait partie de", 'color' : "orange"}
                    nodeDict[datum['processus']['name']] = {'color':"cornflowerblue"}
                    edgeDict[datum['processus']['name'] + '_' + datum['origin']['name']] = {'src': datum['processus']['name'], 'dst': datum['origin']['name'], 'label':"défavorable à", 'color' : "red"}
                    for _agressor in datum['agressors']:
                        agressor_ref = AgressorHelper.agentHelper.rootAgentIdentityBindings[_agressor['name']]
                        nodeDict[_agressor['name']] = {'color':"red"}
                        edgeDict[datum['processus']['name'] + '_' + _agressor['name']] = {'src': datum['processus']['name'], 'dst': _agressor['name'], 'color' : "black"}
                        if _agressor['name'] != agressor_ref['id']['name']:
                            edgeDict[_agressor['name'] + '_' + agressor_ref['id']['name']] = {'src': _agressor['name'], 'dst': agressor_ref['id']['name'], 'label':"fait partie de", 'color' : "orange"}
                        nodeDict[agressor_ref['id']['name']] = {'style' : "filled", 'color' : "red"}
            for key in nodeDict:
                attr = nodeDict[key]
                self.graphBuffer.add_node(pydot.Node(name = key, **attr))
            for key in edgeDict:
                attr = edgeDict[key]
                self.graphBuffer.add_edge(pydot.Edge(**attr))
        return {'query': cypher_query, 'agressors': [v for v in agressors.values()]}
    def findAuxiliary(self, agent, graphic = True):
        agressors = self.findAgressors(agent, False)
        cypher_results = {}
        auxiliaries = {}
        for agressor in agressors['agressors']:
            cypher_results[agressor['id']['name']] = self.agentHelper.findAntiDirectAction(agressor)
            for processus_X_agents in cypher_results[agressor['id']['name']]:
                for _agent in processus_X_agents['agents']:
                    if not _agent['name'] in [v['name'] for v in [agressor['id']] + agressor['subpart']]:
                        ref_agent = self.agentHelper.rootAgentIdentityBindings[_agent['name']]
                        if auxiliaries.get(ref_agent['id']['name']) is None:
                            auxiliaries[ref_agent['id']['name']] = []
                        auxiliaries[ref_agent['id']['name']].append(_agent)
        if graphic:
            self.graphBuffer = pydot.Dot(graph_type='digraph')
            node_dict = {}
            edge_dict = {}
            node_dict[agent['id']['name']] = {'fillcolor' : "green", 'style' : "filled", 'shape': "doubleoctagon"}
            for agressor in agressors['query']:
                for _agent in agressor['agressors']:
                    agressor_ref = self.agentHelper.rootAgentIdentityBindings[_agent['name']]
                    cypher_query = cypher_results[agressor_ref['id']['name']]
                    if not cypher_query:
                        continue
                    node_dict[_agent['name']] = {'color': 'red'}
                    node_dict[agressor_ref['id']['name']] = {'style': "filled", 'fillcolor': "red"}
                    if _agent['name'] != agressor_ref['id']['name']:
                        edge_dict[_agent['name'] + '_' + agressor_ref['id']['name']] = {'src': _agent['name'], 'dst': agressor_ref['id']['name'], 'color': "orange"}
                    edge_dict[_agent['name'] + '_' + agent['id']['name']] = {'src': _agent['name'], 'dst': agent['id']['name'], 'color': "red", 'label': "attack"}
                    for action in cypher_query:
                        node_dict[action['processus']['name']] = {'color': 'cornflowerblue'}
                        edge_dict[action['processus']['name'] + '_' + _agent['name']] = {'src': action['processus']['name'], 'dst':_agent['name'], 'color' : "red"}
                        for auxiliary in action['agents']:
                            if auxiliary['name'] in [v['name'] for v in agressor_ref['subpart']]:
                                continue
                            auxiliary_ref = self.agentHelper.rootAgentIdentityBindings[auxiliary['name']]
                            node_dict[auxiliary['name']] = {'color': 'green'}
                            node_dict[auxiliary_ref['id']['name']] = {'style': "filled", 'fillcolor': "green"}
                            if auxiliary['name'] != auxiliary_ref['id']['name']:
                                edge_dict[auxiliary['name'] + '_' + auxiliary_ref['id']['name']] = {'src': auxiliary['name'], 'dst': auxiliary_ref['id']['name'], 'color': "orange"}
                            edge_dict[action['processus']['name'] + '_' + auxiliary['name']] = {'src': action['processus']['name'], 'dst': auxiliary['name'], 'color': "black"}
            for key in node_dict:
                attr = node_dict[key]
                self.graphBuffer.add_node(pydot.Node(name = key, **attr))
            for key in edge_dict:
                attr = edge_dict[key]
                self.graphBuffer.add_edge(pydot.Edge(**attr)) 
        return auxiliaries



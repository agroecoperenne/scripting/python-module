import py2neo
import re
import pydot
if __package__ is None or __package__ == '':
    # uses current directory visibility
    import Graphable
else:
    # uses current package visibility
    import python_aep_lib.Graphable as Graphable



graph = py2neo.Graph("bolt://138.102.159.59:4205", auth=("neo4j", "pic3.14"))
# graph = py2neo.Graph("bolt://localhost", auth=("neo4j", "pic3.14"))

class AgentHelper(Graphable.Graphable):
    def __init__(self):
        self.agentList = None
        self.rootAgentIdentityBindings = None
        self.getAllAgentAglomerat()
        Graphable.Graphable.__init__(self)
    
    def getAllAgentAglomerat(self):
        if self.agentList is not None:
            result = self.agentList
        else:
            self.rootAgentIdentityBindings = {}
            cypher_query= graph.run(''' 
            MATCH (id) <-[r:part_of*0..]- (any)
            WHERE NOT id:GECO AND (id:LS_Agent OR id:LS_Agent_PP)
            AND (any:LS_Agent OR any:LS_Agent_PP)
            RETURN id , collect(any) AS subpart, length(collect(any)) AS le
            ORDER BY -le
            ''').data()
            used_identities = []
            result = []
            for datum in cypher_query:
                _id = datum['id'].identity
                if _id not in used_identities:
                    nextRes = {"id": datum['id'], "subpart": []}
                    self.rootAgentIdentityBindings[datum['id']['name']] = nextRes
                    used_identities.append(_id)
                    for subnode in datum['subpart']:
                        _subId = subnode.identity
                        if _subId != _id:
                            self.rootAgentIdentityBindings[subnode['name']] = nextRes
                            nextRes['subpart'].append(subnode)
                        used_identities.append(subnode.identity)
                    result.append(nextRes)
            self.agentList = result
        return result
    # Allow multiple world search by separating with '+'
    def findAgent(self, name, regex_search = True):
        all_matches = []
        world_list = name.split('+')
        if not regex_search:
            for agent in self.agentList:
                # if agent['id']['name'] == name:
                if all(agent['id']['name'] == name for  world in world_list):
                    all_matches.append(agent)
                for sub in agent['subpart']:
                    # if sub['name'] == name:
                    if all(sub['name'] == name for  world in world_list):
                        all_matches.append(agent)
        else:
            for agent in self.agentList:
                # pattern = re.compile(name, re.I)
                # if pattern.search(agent['id']['name']):
                # if all(lambda world: re.compile(world, re.I).search(agent['id']['name']) is not None for  world in world_list):
                check = True
                for world in world_list:
                    pattern = re.compile(world, re.I)
                    check = pattern.search(agent['id']['name']) and check
                    if not check:
                        break
                if check:
                    all_matches.append(agent)
                # break
                for sub in agent['subpart']:
                    # if pattern.search(sub['name']):
                    check = True
                    for world in world_list:
                        pattern = re.compile(world, re.I)
                        check = pattern.search(sub['name']) and check
                        if not check:
                            break
                    if check:
                        all_matches.append(agent)
        return all_matches
    
    def getCycle(self, agent, graphic = True):
        agent_names = [v['name'] for v in agent['subpart']]
        stringified_agent_nodes = '["' + '", "'.join(agent_names) +'"]'
        cypher_query= graph.run(''' 
        MATCH (a1) <-[:involve]- (processus:LS_Processus) -[:involve]-> (a2), (processus) -[:depend_on]-> (processus_queue)
        WHERE a1.name IN ''' + stringified_agent_nodes + '''
        AND a2.name IN ''' + stringified_agent_nodes + '''
        RETURN processus, processus_queue
        ''').data()
        all_processus = {}
        if graphic:
            self.graphBuffer = pydot.Dot(graph_type='digraph')
            node_dict = {}
            edge_dict = {}
            node_dict[agent['id']['name']] = {'fillcolor' : "green", 'style' : "filled"}
        for datum in cypher_query:
            pc1 = datum['processus']
            pc2 = datum['processus_queue']
            all_processus[pc1['name']] = pc1
            all_processus[pc2['name']] = pc2
            if graphic:
                edge_dict[pc1['name'] + '_' + pc2['name']] = {'src' : pc1['name'], 'dst': pc2['name'], 'color': "red", 'penwidth': 4}
        cypher_query = graph.run(''' 
        MATCH (agent) <-[:involve]- (processus)
        WHERE processus.name IN ''' + '["' + '", "'.join([key for key in all_processus]) +'"]' + '''
        RETURN agent, processus
        ''').data()
        if graphic:
            for datum in cypher_query:
                _agent = datum['agent']
                processus = datum['processus']
                if _agent['name'] in agent_names:
                    node_dict[_agent['name']] = {'color' : "green"}
                    edge_dict[_agent['name'] + '_' + agent['id']['name']] = {'src': _agent['name'], 'dst': agent['id']['name'], 'color' : "orange"}
                else:
                    node_dict[_agent['name']] = {'color' : "back"}
                node_dict[processus['name']] = {'style' : "filled", 'fillcolor': "cornflowerblue"}
                edge_dict[processus['name'] + '_' + _agent['name']] = {'src': processus['name'], 'dst': _agent['name']}
            for key in node_dict:
                attr = node_dict[key]
                self.graphBuffer.add_node(pydot.Node(name = key, **attr))
            for key in edge_dict:
                attr = edge_dict[key]
                self.graphBuffer.add_edge(pydot.Edge(**attr))        
        return all_processus

    def findDirectAction(self, agent, graphic = True):
        agent_names = [v['name'] for v in agent['subpart']]
        stringified_agent_nodes = '["' + '", "'.join(agent_names) +'"]'
        all_processus = {}
        cypher_query= graph.run(''' 
        MATCH (agent) <-[:involve { role:"defavorable" } ]- (processus:LS_Processus) -[:involve]-> (agricultor)
        WHERE agent.name IN ''' + stringified_agent_nodes + '''
        AND agricultor.name = 'Agriculteur'
        RETURN processus
        ''').data()
        for datum in cypher_query:
            all_processus[datum['processus']['name']] = datum['processus']['name']
        cypher_query= graph.run(''' 
        MATCH (agent) <-[:involve]- (processus:LS_Processus) -[:involve]-> (agricultor)
        WHERE processus.name IN ''' + '["' + '", "'.join([key for key in all_processus]) +'"]' + '''
        AND agricultor.name = 'Agriculteur'
        RETURN processus, collect(agent) as agents
        ''').data()
        if graphic:
            self.graphBuffer = pydot.Dot(graph_type='digraph')
            node_dict = {}
            edge_dict = {}
            node_dict[agent['id']['name']] = {'fillcolor' : "green", 'style' : "filled"}
            for datum in cypher_query:
                for _agent in datum['agents']:
                    if _agent['name'] not in agent_names:
                        agent_ref = self.rootAgentIdentityBindings[_agent['name']]
                        node_dict[_agent['name']] = {'color': 'red'}
                        node_dict[agent_ref['id']['name']] = {'style': "filled", 'fillcolor': "red"}
                        if _agent['name'] != agent_ref['id']['name']:
                            edge_dict[_agent['name'] + '_' + agent_ref['id']['name']] = {'src': _agent['name'], 'dst': agent_ref['id']['name'], 'color': "orange"} 
                    else:
                        if _agent['name'] != agent['id']['name']:
                            node_dict[_agent['name']] = {'color': 'green'}
                            edge_dict[_agent['name'] + '_' + agent['id']['name']] = {'src': _agent['name'], 'dst': agent['id']['name'], 'color': "orange"}
                    edge_dict[datum['processus']['name'] + '_' + _agent['name']] = {'src': datum['processus']['name'], 'dst': _agent['name'], 'color': "black"}
                node_dict[datum['processus']['name']] = {'style': "filled" , "fillcolor": "cornflowerblue"}
            for key in node_dict:
                attr = node_dict[key]
                self.graphBuffer.add_node(pydot.Node(name = key, **attr))
            for key in edge_dict:
                attr = edge_dict[key]
                self.graphBuffer.add_edge(pydot.Edge(**attr))       
        return cypher_query

    def findAntiDirectAction(self, agent):
        agent_names = [v['name'] for v in agent['subpart']]
        stringified_agent_nodes = '["' + '", "'.join(agent_names) +'"]'
        all_processus = {}
        # Cherche les processus défavorable à l'agent
        cypher_query= graph.run(''' 
        MATCH (agent) <-[:involve { role:"defavorable" } ]- (processus:LS_Processus) 
        WHERE agent.name IN ''' + stringified_agent_nodes + '''
        RETURN processus, collect(agent) AS agents
        ''').data()
        for datum in cypher_query:
            all_processus[datum['processus']['name']] = datum['processus']['name']
        # Cherche les processus qui n'ont pas l'agriculteur défavorable à l'agent
        cypher_query= graph.run(''' 
        MATCH (agent) <-[r:involve]- (processus:LS_Processus)
        WHERE processus.name IN ''' + '["' + '", "'.join([key for key in all_processus]) +'"]' + '''
        WITH processus, collect(agent.name) AS agentsName, collect(agent) AS agents
        WHERE not 'Agriculteur' IN agentsName
        RETURN processus, agents
        ''').data()
        result = []
        for datum in cypher_query:
            sub_result = {}
            sub_result['processus'] = datum['processus']
            sub_result['agents'] = list(filter(lambda x: x['name'] not in agent_names, datum['agents'] ))
            result.append(sub_result)
        return cypher_query

    def findIndirectAction(self, agent, graphic = True):
        agent_names = [v['name'] for v in agent['subpart']]
        cypher_query = self.findAntiDirectAction(agent)
        if graphic:
            self.graphBuffer = pydot.Dot(graph_type='digraph')
            node_dict = {}
            edge_dict = {}
            node_dict[agent['id']['name']] = {'fillcolor' : "green", 'style' : "filled"}
            for datum in cypher_query:
                node_dict[datum['processus']['name']] = {'style': "filled" , "fillcolor": "cornflowerblue"}
                for _agent in datum['agents']:
                    if _agent['name'] not in agent_names:
                        agent_ref = self.rootAgentIdentityBindings[_agent['name']]
                        node_dict[_agent['name']] = {'color': 'red'}
                        node_dict[agent_ref['id']['name']] = {'style': "filled", 'fillcolor': "red"}
                        if _agent['name'] != agent_ref['id']['name']:
                            edge_dict[_agent['name'] + '_' + agent_ref['id']['name']] = {'src': _agent['name'], 'dst': agent_ref['id']['name'], 'color': "orange"} 
                    else:
                        if _agent['name'] != agent['id']['name']:
                            node_dict[_agent['name']] = {'color': 'green'}
                            edge_dict[_agent['name'] + '_' + agent['id']['name']] = {'src': _agent['name'], 'dst': agent['id']['name'], 'color': "orange"}
                    edge_dict[datum['processus']['name'] + '_' + _agent['name']] = {'src': datum['processus']['name'], 'dst': _agent['name'], 'color': "black"}
            for key in node_dict:
                attr = node_dict[key]
                self.graphBuffer.add_node(pydot.Node(name = key, **attr))
            for key in edge_dict:
                attr = edge_dict[key]
                self.graphBuffer.add_edge(pydot.Edge(**attr))     
        return cypher_query


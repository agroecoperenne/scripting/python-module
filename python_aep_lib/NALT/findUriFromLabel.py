from SPARQLWrapper import SPARQLWrapper, JSON


sparql = SPARQLWrapper("http://138.102.159.59:4203/sparql")

def findUriFromLabel(label, verbose = False):
    query = f"""
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        SELECT ?uri ?label WHERE {{
            ?uri skos:prefLabel ?label .
            FILTER regex(STR(?label),"{label}","i") 
            FILTER (lang(?label) = 'en')
        }}
    """
    if verbose:
        print(query)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    clean_results = []
    for result in results['results']['bindings']:
        item = {}
        item['uri'] = result['uri']['value']
        item['label'] = result['label']['value']
        clean_results.append(item)
    return clean_results

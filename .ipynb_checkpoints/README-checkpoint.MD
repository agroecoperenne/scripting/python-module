Ce fichier guide l'installation des software pour pouvoir utiliser le module python agroecoperenne.

Afin de stabiliser le code python, nous utiliserons conda (miniconda) et pip pour recréer le même *environnement*.

Au préalable, téléchargez l'ensemble du projet et, le cas échéant, extrayez le à l'emplacement de votre choix.

# Installation de conda

La première étape est d'installer conda, et plus particulièrement miniconda, sur la machine hôte. Pour cela, télécharger la dernière version pour votre OS ici :
https://docs.conda.io/en/latest/miniconda.html

**Attention :** Lors des étapes d'installation il faudra cocher une option pour rajouter conda au PATH du power shell. Sinon il faudra utiliser le Anaconda Power Shell Prompt, et iil est désagréable.
Pour ceux qui ont déja installé miniconda ou Anaconda, cette étape n'est pas nécessaire si la version de conda est au moins 4.6, sinon il faut réaliser une update.

# Création de l'environnement

Une fois conda installé, nous allons créer l'environnement python contenant tous les modules et librairies dont nous aurons besoin.

## Positionnement dans l'arborescence de fichier
Sous Windows 10 (testé), chercher et lancer **Anaconda Powershell Prompt**, puis se positionner à la racine du répertoire contenant le projet.
Sous Ubuntu (testé sous Ubuntu 18), ouvrir un terminal et se positionner à la racine du répertoire contenant le projet.
Sous MacOS (non testé), ouvrir un terminal et se positionner à la racine du répertoire contenant le projet.
### Remarque 1: 
Dans un terminal, la partie à gauche de "là où on tape les instructions" est le chemin actuel, c'est à dire le repertoire courant du point de vue du terminal. Si il y a un mot entre parenthèse, il s'agit de l'environnement courant. 
**Attention :** Dans Anaconda Powershell Prompt, pour windows, il semblerait que le chemin ne soit pas notifié. Dans ce cas, pour vérifier l'emplacemnt actuel, on peut utiliser la commande 'ls'
### Remarque 2:
L'ensemble des commandes dans le bash support l'auto complétion. C'est à dire quand appuyant sur la touche Tabulation, le terminal complètera le chemin en cours si il n'y a pas d'ambiguité, et proposera les choix ambigus sinon.
### Remarque 3:

Pour naviguer dans l'explorateur de fichier à partir d'un terminal parmis ceux listé au dessus, utiliser les commandes suivantes : 

```bash
cd .. # Remonte au parent de l'emplacement actuel dans l'arborescence de fichier
cd ~  # Se place au niveau du home de l'utilisateur actuel
cd /  # Se place à la racine du système de fichier
```

On peut également indiquer tout le chemin (path) vers l'emplacement voulu, si l'on ne commence pas par ~ ou /, le parcours se fait à partir de l'emplacement courant.
Par exemple, pour aller dans un répertoire 'Cible' situé dans Documents de notre HOME, on pourrait écrire:
```bash
cd ~/Documents/Cible
```
si on était déja dans ~/Documents:
```bash
cd Cible
```


## Construction de l'environnement

Une fois à la racine du répertoire contenant le projet, copiez-collez dans le terminal :
```bash
conda env create -f environment.yaml
```

Après quelques instant, un environement nommé **aep** sera créé. Il faudra ensuite l'activé avec 
```bash
conda activate aep
```

Normalement, vous devriez avoir '(aep) ' de préfixé dans votre terminal.

## Importation des derniers packages

La prochaine étape est l'importation des derniers packages par pip. Copiez-collez dans votre terminal:

```bash
pip install -r pip_requirements.txt
```

# Lancement de Jupyter lab

Lancer l'éxécution de Jupyter lab en copiant dans votre terminal (qui est dans l'environnement actif aep):
```bash
jupyter lab
```

# Et ensuite ...?

Une fois les étapes précédentes réaliser, il suffira juste de réactiver l'environnement et de relancer jupyter lab, toujours à la racinde du dossier contenant le projet:
```bash
conda activate aep
jupyter lab
```